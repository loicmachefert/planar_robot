#include "kinematic_model.h"
#include <iostream>
using namespace std;

int main(){
  // Compute the forward kinematics and jacobian matrix for 
  Eigen::Vector2d q; // Define the type of the variable q.
  Eigen::Vector2d q1; // Define the type of the variable q.
  Eigen::Vector2d x; // Define the type of the variable x.
  Eigen::Matrix2d J; // Define the type of the variable J.
 
  double l1; // Define the type of l1.
  double l2; // Define the type of l2.
  
  l1 = 1.0; // Select a value for l1. 
  l2 = 1.0; // Select a value for l2. 

  q << M_PI/3.0,  M_PI/4.0; // Select values for q.
  q1 << M_PI/3.0 + 0.1, M_PI/4.0 + 0.1;

  RobotModel model = RobotModel(l1,l2); // Call the class RobotModel.
  model.FwdKin(x,J,q); // Call the function FwdKin to compute the kinematic model x and the Jacobian matrix J using the values of the joints angles.

  cout << "q = \n" << q << "\n"; // Display the angles values of the joints.
  cout << "x = \n" << x << "\n"; // Display the cartesian position of the end effector.
  cout << "J = \n" << J << "\n"; // Display the Jacobian matrix of the system.

  
  // For a small variation of q, compute the variation on X and check dx = J . dq  
  Eigen::Vector2d dq; // Define the type of the variable dq.
  Eigen::Vector2d dxa; // Define the type of the variable dxa.
  Eigen::Vector2d dxn; // Define the tpe of the value dxn.

  dq << 0.1, 0.1; // Select values for dq.
  dxa = J*dq; // Compute dxa.

  dxn = J*(q1 - q); // Compute dxn.

  cout << "dq = \n" << dq << "\n"; // Display the angles variations of the joints.
  cout << "dx analytic = \n" << dxa << "\n"; // Display the variation dx.
  cout << "dx numeric =  \n" << dxn << "\n";

}
