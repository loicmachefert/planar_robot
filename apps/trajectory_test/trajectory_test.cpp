#include "trajectory_generation.h"
#include <iostream>
using namespace std;

// Compute the trajectory for given initial and final positions.


// Display some of the computed points


// Check numerically the velocities given by the library


// Check initial and final velocities
 
int main()
{
  Eigen::Vector2d X_i (0,0);  // Define the type and the value of the variable X_i.
  Eigen::Vector2d X_f (5,10);  // Define the type and the value of the variable X_f.
  double Dt = 10;  // Define the type and the value of the variable Dt.
  double dt = 1e-6;  // Define the type and the value of th variable dt.

  Point2Point traj = Point2Point(X_i, X_f, Dt);
  Eigen::Vector2d pos;  // Define the type of the value pos.
  Eigen::Vector2d VXA;  // Define the type of the value VXA (Analytic velocity).
  Eigen::Vector2d VXN;  // Define the type of the vaiable VXN (Numeric check).

  for (float t = 0; t<Dt+0.1; t+=0.1) // Computes the position and the velocity of the robot.
  {
    pos = traj.X(t);
    cout << "X :" << pos << "\n\n";
  
    VXA = traj.dX(t);
    cout << "VXA :" << VXA << "\n\n";
    
    VXN = (traj.X(t+dt)-traj.X(t))/dt;
    cout << "VXN :" << VXN << "\n\n";
  }


};